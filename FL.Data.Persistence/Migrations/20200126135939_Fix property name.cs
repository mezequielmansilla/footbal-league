﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FL.Data.Persistence.Migrations
{
    public partial class Fixpropertyname : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Nationnality",
                table: "Players");

            migrationBuilder.AddColumn<string>(
                name: "Nationality",
                table: "Players",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Nationality",
                table: "Players");

            migrationBuilder.AddColumn<string>(
                name: "Nationnality",
                table: "Players",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
