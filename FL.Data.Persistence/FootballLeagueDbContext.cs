﻿using System;
using FL.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace FL.Data.Persistence
{
    public class FootballLeagueDbContext: DbContext
    {
        public DbSet<Area> Areas { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Competition> Competitions { get; set; }
        public DbSet<Player> Players{ get; set; }

        public FootballLeagueDbContext(DbContextOptions<FootballLeagueDbContext> options)
        : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // TeamCompetition relationship
            modelBuilder.Entity<TeamCompetition>()
                .HasKey(bc => new { bc.TeamId, bc.CompetitionId});
            modelBuilder.Entity<TeamCompetition>()
                .HasOne(bc => bc.Team)
                .WithMany(b => b.TeamCompetitions)
                .HasForeignKey(bc => bc.TeamId);
            modelBuilder.Entity<TeamCompetition>()
                .HasOne(bc => bc.Competition)
                .WithMany(c => c.TeamCompetitions)
                .HasForeignKey(bc => bc.CompetitionId);

            // TeamPlayer relationship
            modelBuilder.Entity<TeamPlayer>()
                .HasKey(bc => new { bc.TeamId, bc.PlayerId });
            modelBuilder.Entity<TeamPlayer>()
                .HasOne(bc => bc.Team)
                .WithMany(b => b.TeamPlayers)
                .HasForeignKey(bc => bc.TeamId);
            modelBuilder.Entity<TeamPlayer>()
                .HasOne(bc => bc.Player)
                .WithMany(c => c.TeamPlayers)
                .HasForeignKey(bc => bc.PlayerId);
        }
    }
}
