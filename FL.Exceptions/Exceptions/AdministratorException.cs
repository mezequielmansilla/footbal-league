﻿using System;

namespace FL.Exceptions.Exceptions
{
    public class AdministratorException: Exception
    {
        public AdministratorException()
        {

        }

        public AdministratorException(string message) : base(message)
        {

        }

        public AdministratorException(string message, Exception inner) : base(message, inner)
        {

        }
    }
}
