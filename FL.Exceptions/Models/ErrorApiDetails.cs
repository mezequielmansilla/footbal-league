﻿using Newtonsoft.Json;

namespace FL.Exceptions.Models
{
    public class ErrorApiDetails
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
