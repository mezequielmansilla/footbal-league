﻿using System;
using System.Collections.Generic;

namespace FL.Data.Entities
{
    public class Player: Entity
    {
        public string Name { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string CountryOfBirth { get; set; }
        public string Nationality { get; set; }
        public string Position { get; set; }
        public ICollection<TeamPlayer> TeamPlayers { get; set; }
    }
}
