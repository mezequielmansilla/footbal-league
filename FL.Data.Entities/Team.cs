﻿using System.Collections.Generic;

namespace FL.Data.Entities
{
    public class Team: Entity
    {
        public string Tla { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string Email { get; set; }
        public Area Area { get; set; }
        public ICollection<TeamCompetition> TeamCompetitions { get; set; }
        public ICollection<TeamPlayer> TeamPlayers { get; set; }
    }
}
