﻿using System.Collections.Generic;

namespace FL.Data.Entities
{
    public class Competition: Entity
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public Area Area { get; set; }
        public ICollection<TeamCompetition> TeamCompetitions { get; set; }
       
    }
}
