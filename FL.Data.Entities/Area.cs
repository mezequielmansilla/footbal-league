﻿using System.Collections.Generic;

namespace FL.Data.Entities
{
    public class Area: Entity
    {
        public string Name { get; set; }
        public ICollection<Competition> Competitions { get; set; }
        public ICollection<Team> Teams { get; set; }
    }
}
