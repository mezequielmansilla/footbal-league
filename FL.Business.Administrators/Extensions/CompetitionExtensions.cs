﻿using FL.Data.Entities;
using System;
using System.Collections.Generic;

namespace FL.Business.Administrators.Extensions
{
    public static class CompetitionExtensions
    {
        public static void AddTeam(this Competition competition, Team team)
        {
            try
            {
                if (team == null)
                {
                    throw new ArgumentNullException("team");
                }
                var teamCompetition = new TeamCompetition();
                teamCompetition.Team = team;
                teamCompetition.Competition = competition;
                if (competition.TeamCompetitions == null)
                {
                    competition.TeamCompetitions = new List<TeamCompetition>();
                }
                competition.TeamCompetitions.Add(teamCompetition);
            }
            catch(Exception e)
            {
                throw new Exception("Error adding a team to competition", e);
            }
            
        }

        public static void AddTeams(this Competition competition, List<Team> teams)
        {
            try
            {
                if (teams == null)
                {
                    throw new ArgumentNullException("teams");
                }
                teams.ForEach(team => competition.AddTeam(team));
            }
            catch (Exception e)
            {
                throw new Exception("Error adding teams to competition", e);
            }
        }
    }
}
