﻿using FL.Data.Entities;
using System;
using System.Collections.Generic;

namespace FL.Business.Administrators.Extensions
{
    public static class TeamExtensions
    {
        public static void AddPlayer(this Team team, Player player)
        {
            try
            {
                if (player == null)
                {
                    throw new ArgumentNullException("player");
                }
                var teamPlayer = new TeamPlayer();
                teamPlayer.Player = player;
                teamPlayer.Team = team;
                if (team.TeamPlayers == null)
                {
                    team.TeamPlayers = new List<TeamPlayer>();
                }
                team.TeamPlayers.Add(teamPlayer);
            } 
            catch(Exception e)
            {
                throw new Exception("Error adding player to a team", e);
            }
            
        }

        public static void AddPlayers(this Team team, List<Player> players)
        {
            try
            {
                if (players == null)
                {
                    throw new ArgumentNullException("players");
                }
                players.ForEach(player => team.AddPlayer(player));
            }
            catch (Exception e)
            {
                throw new Exception("Error adding players to a team", e);
            }
        }
    }
}
