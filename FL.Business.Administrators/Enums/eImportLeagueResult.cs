﻿namespace FL.Business.Administrators.Enums
{
    public enum eImportLeagueResult
    {
        Successfull,
        AlreadyImported,
        LeagueNotFound,
    }
}
