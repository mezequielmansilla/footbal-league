﻿using FL.Business.Administrators.Enums;
using FL.Data.Entities;
using System.Threading.Tasks;

namespace FL.Business.Administrators.ServiceContracts
{
    public interface ILeagueAdministrator
    {
        /// <summary>
        /// Import a complete league into the database
        /// </summary>
        /// <param name="competition"></param>
        /// <returns></returns>
        Task<eImportLeagueResult> ImportLeagueByLeagueCode(string leagueCode);
        /// <summary>
        /// Return the total amount of players belonging to all teams that participate in the given league (leagueCode).
        /// Return -1 if the league doesn't exist
        /// </summary>
        /// <param name="leagueCode"></param>
        /// <returns>Return -1 if the league doesn't exist</returns>.
        Task<int> GetTotalPlayersByLeagueCode(string leagueCode);
    }
}
