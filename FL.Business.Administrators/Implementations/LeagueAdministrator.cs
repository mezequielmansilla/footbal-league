﻿using FL.Business.Administrators.Enums;
using FL.Business.Administrators.Extensions;
using FL.Business.Administrators.ServiceContracts;
using FL.Data.Entities;
using FL.Data.FootballAPI;
using FL.Data.Repositories.ServiceContracts;
using FL.Exceptions.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FL.Business.Administrators.Implementations
{
    public class LeagueAdministrator: ILeagueAdministrator
    {
        private readonly IAreaRepository _areaRepository;
        private readonly ICompetitionRepository _competitionRepository;
        private readonly ITeamRepository _teamRepository;
        private readonly IPlayerRepository _playerRepository;
        private readonly IFootballDataApi _footballDataApi;

        public LeagueAdministrator(IFootballDataApi footballDataApi, IPlayerRepository playerRepository, ITeamRepository teamRepository, ICompetitionRepository competitionRepository, IAreaRepository areaRepository)
        {
            _playerRepository = playerRepository;
            _teamRepository = teamRepository;
            _competitionRepository = competitionRepository;
            _areaRepository = areaRepository;
            _footballDataApi = footballDataApi;
        }

        /// <summary>
        /// Check if a league exists in database.
        /// </summary>
        /// <param name="leagueCode"></param>
        private async Task<bool> ExistLeagueInDataBaseByLeagueCode(string leagueCode)
        {
            try
            {
                var competition = await _competitionRepository.GetByCode(leagueCode);
                if (competition != null)
                {
                    return true;
                }
                return false;
            }
            catch(Exception e)
            {
                throw new AdministratorException("Error checking league in DB.", e);
            }
        }

        public async Task<int> GetTotalPlayersByLeagueCode(string leagueCode)
        {
            try
            {
                var count = 0;
                if (await ExistLeagueInDataBaseByLeagueCode(leagueCode) == false)
                {
                    return -1;
                }
                var teamsByLeague = _teamRepository.GetTeamsByLeagueCode(leagueCode);
                if (teamsByLeague != null && teamsByLeague.Count() > 0)
                {
                    var teamsId = teamsByLeague.Select(team => team.Id).ToArray();
                    var playerByTeams = _playerRepository.GetPlayersByTeams(teamsId);
                    count = playerByTeams.Count();
                }
                return count;
            }
            catch(Exception e)
            {
                throw new AdministratorException("Error checking league in DB.", e);
            }
        }

        public async Task<eImportLeagueResult> ImportLeagueByLeagueCode(string leagueCode)
        {
            try
            {
                if (leagueCode == null || leagueCode == "")
                {
                    throw new ArgumentException("leagueCode");
                }
                if (await ExistLeagueInDataBaseByLeagueCode(leagueCode) == true)
                {
                    // The league was already imported
                    return eImportLeagueResult.AlreadyImported;
                }

                var competitionsList = _footballDataApi.GetCompetitions();
                if (competitionsList == null)
                {
                    throw new AdministratorException("Error in external API");
                }
                var competition = competitionsList.Competitions != null ? competitionsList.Competitions.FirstOrDefault(c => c.Code == leagueCode) : null;
                if (competition == null)
                {
                    // The league doesn't exist
                    return eImportLeagueResult.LeagueNotFound;
                }
                var teams = _footballDataApi.GetTeamsByCompetition(competition.Id);
                var players = new List<Player>();
                teams.Teams.ForEach(team =>
                {
                    var teamPlayers = _footballDataApi.GetTeamDetail(team.Id);
                    if (teamPlayers.Squad != null)
                    {
                        team.AddPlayers(teamPlayers.Squad);
                    }
                    competition.AddTeam(team);
                });
                await _competitionRepository.Create(competition);
                return eImportLeagueResult.Successfull;
            }
            catch (Exception e)
            {
                throw new AdministratorException("Error importing league.", e);
            }
        }
    }
}
