﻿using FL.Business.Administrators.Implementations;
using FL.Business.Administrators.ServiceContracts;
using FL.Data.Entities;
using FL.Data.FootballAPI;
using FL.Data.Repositories.ServiceContracts;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace FL.Business.Administrators.Tests.LeagueAdministratorTests
{
    public class LeagueAdministrator_GetTotalPlayersByLeagueCode
    {
        private ILeagueAdministrator _leagueAdministrator;
        private Mock<ICompetitionRepository>  _competitionRepositoryMock;
        private Mock<IAreaRepository> _areaRepositoryMock;
        private Mock<IPlayerRepository> _playerRepositoryMock;
        private Mock<ITeamRepository> _teamRepositoryMock;
        private Mock<IFootballDataApi> _footballDataApiMock;

        public LeagueAdministrator_GetTotalPlayersByLeagueCode()
        {
            
        }

        public void SetUp()
        {
            _competitionRepositoryMock = new Mock<ICompetitionRepository>();
            _areaRepositoryMock = new Mock<IAreaRepository>();
            _playerRepositoryMock = new Mock<IPlayerRepository>();
            _teamRepositoryMock = new Mock<ITeamRepository>();
            _footballDataApiMock = new Mock<IFootballDataApi>();

            _leagueAdministrator = new LeagueAdministrator(
                _footballDataApiMock.Object,
                _playerRepositoryMock.Object,
                _teamRepositoryMock.Object,
                _competitionRepositoryMock.Object,
                _areaRepositoryMock.Object);
        }

        [Fact]
        public async void ShouldReturnNotFound_WhenLeagueNotExists()
        {
            SetUp();
            var totalPlayers = await _leagueAdministrator.GetTotalPlayersByLeagueCode("WC");
            Assert.Equal<int>(-1, totalPlayers);
        }

        [Fact]
        public async void ShoulReturnZero_WhenThereAreNotTeams()
        {
            SetUp();
            var emptyTeamList = new List<Team>();
            var competitionMock = new Mock<Competition>();
            _teamRepositoryMock.Setup(p => p.GetTeamsByLeagueCode(It.IsAny<string>())).Returns(emptyTeamList.AsQueryable());
            _competitionRepositoryMock.Setup(p => p.GetByCode(It.IsAny<string>())).Returns(Task.FromResult(competitionMock.Object));

            var totalPlayers = await _leagueAdministrator.GetTotalPlayersByLeagueCode("WC");

            Assert.Equal<int>(0, totalPlayers);
        }

        [Fact]
        public async void ShoulReturnZero_WhenThereAreNotPlayers()
        {
            SetUp();
            var teamList = new List<Team>()
            {
                new Mock<Team>().Object
            };
            var emptyPlayerList = new List<Player>();
            var competitionMock = new Mock<Competition>();
            _teamRepositoryMock.Setup(p => p.GetTeamsByLeagueCode(It.IsAny<string>())).Returns(teamList.AsQueryable());
            _competitionRepositoryMock.Setup(p => p.GetByCode(It.IsAny<string>())).Returns(Task.FromResult(competitionMock.Object));
            _playerRepositoryMock.Setup(p => p.GetPlayersByTeams(It.IsAny<int[]>())).Returns(emptyPlayerList.AsQueryable());
            var totalPlayers = await _leagueAdministrator.GetTotalPlayersByLeagueCode("WC");

            Assert.Equal<int>(0, totalPlayers);
        }

        [Fact]
        public async void ShoulReturnCorrectTwo()
        {
            SetUp();
            var teamList = new List<Team>()
            {
                new Mock<Team>().Object
            };
            var playerList = new List<Player>()
            {
                new Mock<Player>().Object,
                new Mock<Player>().Object
            };
            var competitionMock = new Mock<Competition>();
            _teamRepositoryMock.Setup(p => p.GetTeamsByLeagueCode(It.IsAny<string>())).Returns(teamList.AsQueryable());
            _competitionRepositoryMock.Setup(p => p.GetByCode(It.IsAny<string>())).Returns(Task.FromResult(competitionMock.Object));
            _playerRepositoryMock.Setup(p => p.GetPlayersByTeams(It.IsAny<int[]>())).Returns(playerList.AsQueryable());
            var totalPlayers = await _leagueAdministrator.GetTotalPlayersByLeagueCode("WC");

            Assert.Equal<int>(2, totalPlayers);
        }
    }
}
