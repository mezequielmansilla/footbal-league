﻿using FL.Business.Administrators.Enums;
using FL.Business.Administrators.Implementations;
using FL.Business.Administrators.ServiceContracts;
using FL.Data.Entities;
using FL.Data.FootballAPI;
using FL.Data.FootballAPI.Models;
using FL.Data.Repositories.ServiceContracts;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace FL.Business.Administrators.Tests.LeagueAdministratorTests
{
    public class LeagueAdministrator_ImportLeagueByLeagueCode
    {
        private ILeagueAdministrator _leagueAdministrator;
        private Mock<ICompetitionRepository> _competitionRepositoryMock;
        private Mock<IAreaRepository> _areaRepositoryMock;
        private Mock<IPlayerRepository> _playerRepositoryMock;
        private Mock<ITeamRepository> _teamRepositoryMock;
        private Mock<IFootballDataApi> _footballDataApiMock;

        public void SetUp()
        {
            _competitionRepositoryMock = new Mock<ICompetitionRepository>();
            _areaRepositoryMock = new Mock<IAreaRepository>();
            _playerRepositoryMock = new Mock<IPlayerRepository>();
            _teamRepositoryMock = new Mock<ITeamRepository>();
            _footballDataApiMock = new Mock<IFootballDataApi>();

            _leagueAdministrator = new LeagueAdministrator(
                _footballDataApiMock.Object,
                _playerRepositoryMock.Object,
                _teamRepositoryMock.Object,
                _competitionRepositoryMock.Object,
                _areaRepositoryMock.Object);
        }

        [Fact]
        public async void ShoudReturnLeagueAlreadyImported_WhenLeagueNotExist()
        {
            SetUp();
            var competitionMock = new Mock<Competition>();
            _competitionRepositoryMock.Setup(p => p.GetByCode(It.IsAny<string>())).Returns(Task.FromResult(competitionMock.Object));
            var importResult = await _leagueAdministrator.ImportLeagueByLeagueCode("WC");

            Assert.Equal(eImportLeagueResult.AlreadyImported, importResult);
        }

        [Fact]
        public async void ShoudReturnLeagueNotFound_WhenLeagueNotExistInFootballDataApi()
        {
            SetUp();
            var competitions = new List<Competition>();
            var listResponseMock = new Mock<CompetitionListResponse>();
            _footballDataApiMock.Setup(p => p.GetCompetitions()).Returns(listResponseMock.Object);
            var importResult = await _leagueAdministrator.ImportLeagueByLeagueCode("WC");

            Assert.Equal(eImportLeagueResult.LeagueNotFound, importResult);
        }

        [Fact]
        public async void ShoudReturnSuccessfull()
        {
            SetUp();
            var competitionMock = new Mock<Competition>();
            var competitions = new List<Competition>() {
                new Competition()
                {
                    Code = "WC"
                }
            };
            var competitionListResponse = new CompetitionListResponse();
            competitionListResponse.Competitions = competitions;

            var teams = new List<Team>();
            var teamListResponse = new CompetitionTeamListResponse();
            teamListResponse.Teams = teams;

            _footballDataApiMock.Setup(p => p.GetCompetitions()).Returns(competitionListResponse);
            _footballDataApiMock.Setup(p => p.GetTeamsByCompetition(It.IsAny<int>())).Returns(teamListResponse);

            var importResult = await _leagueAdministrator.ImportLeagueByLeagueCode("WC");

            Assert.Equal(eImportLeagueResult.Successfull, importResult);
        }
    }
}
