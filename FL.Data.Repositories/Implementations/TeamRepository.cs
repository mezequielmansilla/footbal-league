﻿using FL.Data.Persistence;
using FL.Data.Entities;
using FL.Data.Repositories.ServiceContracts;
using System.Linq;

namespace FL.Data.Repositories.Implementations
{
    public class TeamRepository: EFRepository<Team>, ITeamRepository
    {
        public TeamRepository(FootballLeagueDbContext dbContext) : base(dbContext)
        {
        }

        public IQueryable<Team> GetTeamsByLeagueCode(string leagueCode)
        {
            return GetAll().Where(team => team.TeamCompetitions.Any(teamCompetition => teamCompetition.Competition.Code == leagueCode));
        }
    }
}
