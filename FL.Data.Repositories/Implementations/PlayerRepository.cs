﻿using FL.Data.Persistence;
using FL.Data.Entities;
using FL.Data.Repositories.ServiceContracts;
using System.Linq;

namespace FL.Data.Repositories.Implementations
{
    public class PlayerRepository: EFRepository<Player>, IPlayerRepository
    {
        public PlayerRepository(FootballLeagueDbContext dbContext) : base(dbContext)
        {
        }

        public IQueryable<Player> GetPlayersByTeam(int teamId)
        {
            return GetAll()
                .Where(player => player.TeamPlayers.Any(teamPlayer => teamPlayer.TeamId == teamId));
        }

        public IQueryable<Player> GetPlayersByTeams(int[] teamsId)
        {
            return GetAll()
                .Where(player => player.TeamPlayers.Any(teamPlayer => teamsId.Contains(teamPlayer.TeamId)));
        }
    }
}
