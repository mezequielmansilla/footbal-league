﻿using FL.Data.Persistence;
using FL.Data.Entities;
using FL.Data.Repositories.ServiceContracts;

namespace FL.Data.Repositories.Implementations
{
    public class AreaRepository: EFRepository<Area>, IAreaRepository
    {
        public AreaRepository(FootballLeagueDbContext dbContext): base(dbContext)
        {
        }
    }
}
