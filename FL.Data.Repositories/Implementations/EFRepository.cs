﻿using FL.Data.Persistence;
using FL.Data.Entities;
using FL.Data.Repositories.ServiceContracts;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using FL.Exceptions;
using System;

namespace FL.Data.Repositories.Implementations
{
    public class EFRepository<TEntity> : IEFRepository<TEntity> where TEntity : Entity
    {
        private readonly FootballLeagueDbContext _dbContext;

        public EFRepository(FootballLeagueDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task Create(TEntity entity)
        {
            try
            {
                //_dbContext.Database.ExecuteSqlRaw($"SET IDENTITY_INSERT dbo.{typeof(TEntity).Name} ON;");
                await _dbContext.Set<TEntity>().AddAsync(entity);
                await _dbContext.SaveChangesAsync();
                //_dbContext.Database.ExecuteSqlRaw($"SET IDENTITY_INSERT dbo.{typeof(TEntity).Name} OFF;");
            }
            catch(Exception e)
            {
                throw new RepositoryException("Error when creating entity", e);
            }
        }

        public async Task Update(int id, TEntity entity)
        {

            try
            {
                _dbContext.Set<TEntity>().Update(entity);
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                throw new RepositoryException("Error when updating entity", e);
            }
        }

        public async Task Delete(int id)
        {
            try
            {
                var entity = await GetById(id);
                _dbContext.Set<TEntity>().Remove(entity);
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                throw new RepositoryException("Error when deleting entity", e);
            }
        }

        public IQueryable<TEntity> GetAll()
        {
            try
            {
                return _dbContext.Set<TEntity>().AsNoTracking();
            }
            catch (Exception e)
            {
                throw new RepositoryException("Error when getting all entities", e);
            }
        }

        public async Task<TEntity> GetById(int id)
        {
            try
            {
                return await _dbContext.Set<TEntity>()
                .AsNoTracking()
                .FirstOrDefaultAsync(e => e.Id == id);
            }
            catch (Exception e)
            {
                throw new RepositoryException("Error when getting the entity by id", e);
            }

        }
    }
}
