﻿using FL.Data.Persistence;
using FL.Data.Entities;
using FL.Data.Repositories.ServiceContracts;
using System.Threading.Tasks;
using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace FL.Data.Repositories.Implementations
{
    public class CompetitionRepository: EFRepository<Competition>, ICompetitionRepository
    {
        public CompetitionRepository(FootballLeagueDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<Competition> GetByCode(string leagueCode)
        {
            if (leagueCode == null || leagueCode == "")
            {
                throw new ArgumentNullException("leagueCode");
            }
            return await GetAll().FirstOrDefaultAsync(competition => competition.Code == leagueCode);
        }
    }
}
