﻿using FL.Data.Entities;
using System.Threading.Tasks;

namespace FL.Data.Repositories.ServiceContracts
{
    public interface ICompetitionRepository: IEFRepository<Competition>
    {
        Task<Competition> GetByCode(string leagueCode);
    }
}
