﻿using FL.Data.Entities;
using System.Linq;

namespace FL.Data.Repositories.ServiceContracts
{
    public interface IPlayerRepository: IEFRepository<Player>
    {
        IQueryable<Player> GetPlayersByTeam(int teamId);
        IQueryable<Player> GetPlayersByTeams(int[] teamsId);
    }
}
