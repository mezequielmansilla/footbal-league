﻿using FL.Data.Entities;
using System.Linq;
using System.Threading.Tasks;

namespace FL.Data.Repositories.ServiceContracts
{
    public interface IEFRepository<TEntity> where TEntity : Entity
    {
        IQueryable<TEntity> GetAll();
        Task<TEntity> GetById(int id);
        Task Create(TEntity entity);
        Task Update(int id, TEntity entity);
        Task Delete(int id);
    }
}
