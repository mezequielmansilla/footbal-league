﻿using FL.Data.Entities;
using System.Linq;

namespace FL.Data.Repositories.ServiceContracts
{
    public interface ITeamRepository: IEFRepository<Team>
    {
        IQueryable<Team> GetTeamsByLeagueCode(string leagueCode);
    }
}
