﻿using FL.Data.Entities;

namespace FL.Data.Repositories.ServiceContracts
{
    public interface IAreaRepository : IEFRepository<Area>
    {
    }
}
