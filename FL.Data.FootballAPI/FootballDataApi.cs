﻿using FL.Data.FootballAPI.Models;
using FL.Exceptions.Exceptions;
using Microsoft.Extensions.Configuration;
using RestSharp;
using RestSharp.Authenticators;
using System;

namespace FL.Data.FootballAPI
{
    public class FootballDataApi: IFootballDataApi
    {
        private string FootballDataApiUrl;

        public FootballDataApi(IConfiguration configuration)
        {
            Configuration = configuration;
            FootballDataApiUrl = Configuration.GetSection("FootballDataApi").GetSection("Url").Value;
        }

        public IConfiguration Configuration { get; }

        private IAuthenticator GetAuthenticator()
        {
            return new FootballAuthenticator(Configuration.GetSection("FootballDataApi").GetSection("Token").Value);
        }

        public CompetitionListResponse GetCompetitions()
        {
            try
            {
                var client = new RestClient(FootballDataApiUrl);
                client.Authenticator = GetAuthenticator();
                var url = "/competitions";
                var request = new RestRequest(url, DataFormat.Json);
                var response = client.Get<CompetitionListResponse>(request);
                return response.Data;
            } 
            catch(Exception e)
            {
                throw new ApiException("Error calling external api football-data/competitions.", e);
            }
        }

        public TeamDetailResponse GetTeamDetail(int teamId)
        {
            try
            {
                var client = new RestClient(FootballDataApiUrl);
                client.Authenticator = GetAuthenticator();
                var url = $"/teams/{teamId}";
                var request = new RestRequest(url, DataFormat.Json);
                var response = client.Get<TeamDetailResponse>(request);
                return response.Data;
            }
            catch (Exception e)
            {
                throw new ApiException("Error calling external api football-data/teams.", e);
            }
        }

        public CompetitionTeamListResponse GetTeamsByCompetition(int competitionId)
        {
            try
            {
                var client = new RestClient(FootballDataApiUrl);
                client.Authenticator = GetAuthenticator();
                var url = $"/competitions/{competitionId}/teams";
                var request = new RestRequest(url, DataFormat.Json);
                var response = client.Get<CompetitionTeamListResponse>(request);
                return response.Data;
            }
            catch (Exception e)
            {
                throw new ApiException("Error calling external api football-data/competitions/teams.", e);
            }

        }
    }
}
