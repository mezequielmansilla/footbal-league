﻿using FL.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace FL.Data.FootballAPI.Models
{
    public class CompetitionListResponse: ListResponse
    {
        public List<Competition> Competitions { get; set; }
    }
}
