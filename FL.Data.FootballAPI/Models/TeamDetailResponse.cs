﻿using FL.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace FL.Data.FootballAPI.Models
{
    public class TeamDetailResponse
    {
        public List<Competition> activeCompetitions  { get; set; }
        public List<Player> Squad { get; set; }
    }
}
