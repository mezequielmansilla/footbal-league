﻿using FL.Data.Entities;
using System.Collections.Generic;

namespace FL.Data.FootballAPI.Models
{
    public class CompetitionTeamListResponse: ListResponse
    {
        public Competition Competition { get; set; }
        public List<Team> Teams { get; set; }
    }
}
