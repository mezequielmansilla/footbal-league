﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FL.Data.FootballAPI.Models
{
    public class ListResponse
    {
        public int Count { get; set; }
    }
}
