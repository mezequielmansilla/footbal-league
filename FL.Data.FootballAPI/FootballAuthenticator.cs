﻿using RestSharp;
using RestSharp.Authenticators;

namespace FL.Data.FootballAPI
{
    public class FootballAuthenticator : AuthenticatorBase
    {
        public FootballAuthenticator(string accessToken): base(accessToken)
        {

        }
        protected override Parameter GetAuthenticationParameter(string accessToken)
        {
            return new Parameter("X-Auth-Token", accessToken, ParameterType.HttpHeader);
        }
    }
}
