﻿using FL.Data.Entities;
using FL.Data.FootballAPI.Models;

namespace FL.Data.FootballAPI
{
    public interface IFootballDataApi
    {
        CompetitionListResponse GetCompetitions();
        CompetitionTeamListResponse GetTeamsByCompetition(int competitionId);
        TeamDetailResponse GetTeamDetail(int teamId);
    }
}
