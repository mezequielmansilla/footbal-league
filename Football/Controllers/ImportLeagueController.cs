﻿using System;
using System.Threading.Tasks;
using FL.API.Models;
using FL.Business.Administrators.Enums;
using FL.Business.Administrators.ServiceContracts;
using Microsoft.AspNetCore.Mvc;

namespace FL.API.Controllers
{
    [Route("import-league")]
    public class ImportLeagueController : FootballLeagueBaseController
    {
        private readonly ILeagueAdministrator _leagueAdministrator;

        public ImportLeagueController(ILeagueAdministrator leagueAdministrator)
        {
            _leagueAdministrator = leagueAdministrator;
        }

        [HttpGet]
        [Route("{leagueCode}")]
        public async Task<ActionResult<ImportLeagueResponse>> ImportLeague(string leagueCode)
        {
            try
            {
                var importResult = await _leagueAdministrator.ImportLeagueByLeagueCode(leagueCode);
                switch(importResult)
                {
                    case eImportLeagueResult.Successfull:
                        return StatusCode(201, new ImportLeagueResponse(201));
                    case eImportLeagueResult.LeagueNotFound:
                        return StatusCode(404, new ImportLeagueResponse(404));
                    case eImportLeagueResult.AlreadyImported:
                        return StatusCode(409, new ImportLeagueResponse(409));
                }
                return StatusCode(504, new ImportLeagueResponse(504));
            }
            catch(Exception e)
            {
                return StatusCode(504, new ImportLeagueResponse(504));
            }
            
        }
    }
}