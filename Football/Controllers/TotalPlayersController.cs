﻿using System;
using System.Threading.Tasks;
using FL.API.Models;
using FL.Business.Administrators.ServiceContracts;
using Microsoft.AspNetCore.Mvc;

namespace FL.API.Controllers
{
    [Route("total-players")]
    public class TotalPlayersController : FootballLeagueBaseController
    {
        private readonly ILeagueAdministrator _leagueAdministrator;
        public TotalPlayersController(ILeagueAdministrator leagueAdministrator)
        {
            _leagueAdministrator = leagueAdministrator;
        }

        [HttpGet]
        [Route("{leagueCode}")]
        public async Task<ActionResult<TotalPlayersResponse>> GetTotalPlayersByLeague(string leagueCode)
        {
            try
            {
                var count = await _leagueAdministrator.GetTotalPlayersByLeagueCode(leagueCode);
                if (count == -1)
                {
                    return NotFound();
                }
                return new TotalPlayersResponse(count);
            }
            catch (Exception e)
            {
                return new TotalPlayersResponse(0);
            }
        }
    }
}