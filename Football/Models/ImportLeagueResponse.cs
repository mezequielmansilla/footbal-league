﻿namespace FL.API.Models
{
    public class ImportLeagueResponse
    {
        public string message { get; set; }
        public ImportLeagueResponse(int statusCode)
        {
            switch(statusCode)
            {
                case 201:
                    message = "Successfully imported";
                    break;
                case 409:
                    message = "League already imported";
                    break;
                case 404:
                    message = "Not found";
                    break;
                case 504:
                    message = "Server Error";
                    break;
            }
        }
    }
}
