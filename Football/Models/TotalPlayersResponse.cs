﻿namespace FL.API.Models
{
    public class TotalPlayersResponse
    {
        public int total { get; set; }
        public TotalPlayersResponse(int total)
        {
            this.total = total;
        }
    }
}
